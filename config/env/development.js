module.exports = {
  port: 80,
  models: {
    connection: 'dbDev'
  },
  db_conf: {
    host: 'localhost',
    user: 'car',
    password: 'car_web',
    database: 'Car-dev',
    port: 5432
  }
};
