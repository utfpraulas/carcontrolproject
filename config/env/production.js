module.exports = {
  models: {
    connection: 'carControlProd'
  },
  port: 80,
  session: {
    adapter: 'connect-redis',
    host: 'carControlct.com.br',
    port: 6379,
    ttl: 86400,
    db: 0,
    pass: "postgres",
    prefix: 'carControl-sess:',
    key: 'carControl'
  },
  db_conf: {
    host: 'postgres-prod',
    user: 'local',
    password: 'postgres',
    database: 'carControl-producao',
    port: 5432
  }
};
