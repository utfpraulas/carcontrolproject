CREATE SEQUENCE public.utfprvisitantes_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."utfprvisitante"
(
    nome text NOT NULL COLLATE pg_catalog."default",
	cpf text NOT NULL COLLATE pg_catalog."default",
	celular integer NOT NULL,
	email text NOT NULL COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('utfprvisitantes_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT utfprvisitantes_pkey PRIMARY KEY (id),
    CONSTRAINT utfprvisitantes_cpf_key UNIQUE (cpf),
	CONSTRAINT utfprvisitantes_celular_key UNIQUE (celular),
	CONSTRAINT utfprvisitantes_email_key UNIQUE (email)
)