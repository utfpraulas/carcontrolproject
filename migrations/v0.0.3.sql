CREATE SEQUENCE public.veiculo_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;
CREATE TABLE public."veiculo"
(
    tipo text NOT NULL COLLATE pg_catalog."default",
    modelo text NOT NULL COLLATE pg_catalog."default",
    fabricante text NOT NULL COLLATE pg_catalog."default",
	placa text NOT NULL COLLATE pg_catalog."default",
	cor text NOT NULL COLLATE pg_catalog."default",
    id integer NOT NULL DEFAULT nextval('veiculo_id_seq'::regclass),
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    CONSTRAINT veiculo_pkey PRIMARY KEY (id),
    CONSTRAINT veiculo_placa_key UNIQUE (placa)
)