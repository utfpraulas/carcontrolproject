module.exports = {
    attributes: {
      tipo: { type: 'string' },
      modelo: { type: 'string' },
      fabricante: { type: 'string' },
      placa: { type: 'string' },
      cor: { type: 'string' }
    }
  };