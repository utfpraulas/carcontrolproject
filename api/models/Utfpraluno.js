module.exports = {
    attributes: {
      nome: { type: 'string' },
      curso: { type: 'string' },
      ra: { type: 'integer' },
      celular: { type: 'integer' },
      email: { type: 'string' }
    }
  };