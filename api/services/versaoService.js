let versaoDB = "1.0.0";
let appMin = "1.0.0";

module.exports = {
	getVersao: () => {
		return require("../../package.json").version;
	},
	getDB: () => {
		return versaoDB;
	},
	getAppMin: () => {
		return appMin;
	},
	getVersaoDB: (cb) => {
		Db.find().sort('id DESC').limit(1).exec( (err, versao) => {
			if (versao){
				versaoDB = versao[0].versao;
				appMin = versao[0].appMin;
				return cb(versao);
			} 
			return cb(0);
		})
	}
};