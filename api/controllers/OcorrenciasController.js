module.exports = {
    new: (req, res) => {
            Utfprestacionamento.find().exec((err, est) => {
                return res.view({estacionamentos: est });
            });
    },
    index: (req, res) => {
        let veiculo = req.param("veiculosel")
        if (veiculo) {
            Veiculo.find().exec((err, vec) => {
                Ocorrencias.find({ id_veiculo: veiculo }).exec((err, oco) => {
                    return res.view({ ocorrencias: oco, veiculos: vec });
                });
            });
        } else {
            Veiculo.find().exec((err, vec) => {
                return res.view({ ocorrencias: null, veiculos: vec });
            });
        }
    },
    create: (req, res) => {
        var o = {
            id_veiculo: req.param('veiculo'),
            id_estacionamento: req.param('estacionamento'),
            observacao: req.param('observacao'),
        };
        Ocorrencias.create(o, (err, oco) => {

            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar a Ocorrencia, info:`, 'error');
                return res.redirect('/ocorrencias/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/ocorrencias');
            }
        });
    },
};

