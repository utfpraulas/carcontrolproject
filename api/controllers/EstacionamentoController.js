module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    index: (req, res) => {
        Utfprestacionamento.find({}).exec((err, estacionamento) => {
            return res.view({ estacionamentos: estacionamento });
        });
    },
    create: (req, res) => {
        var e = {
            descricao: req.param('descricao'),
            codigo: req.param('codigo')
        };
        Utfprestacionamento.create(e, (err, esta) => {

            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar a Estacionamento, info:`, 'error');
                return res.redirect('/estacionamento/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/estacionamento');
            }
        });
    },
};

