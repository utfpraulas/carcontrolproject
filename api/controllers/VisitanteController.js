module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    index: (req, res) => {
        Utfprvisitante.find().exec((err, vis) => {
            return res.view({ visitantes: vis });
        });
    },
    create: (req, res) => {

        var v = {
            nome: req.param('nome'),
            cpf: req.param('cpfcnpj'),
            celular: req.param('celular'),
            email: req.param('email'),
        };
        Utfprvisitante.create(v, (err, vizi) => {
            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar a Visitante, info:`, 'error');
                return res.redirect('/visitante/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/visitante');
            }
        });
    },
};

