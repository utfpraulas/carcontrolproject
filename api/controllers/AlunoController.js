module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    edit: (req, res) => {
        let id = req.param('id');
        let isEdit = req.param("is_edit");
            
        Utfpraluno.find(id).exec((err, alu) => {
            if (!isEdit) {
                return res.view({ aluno: alu[0], editavel: false });
            } else {
                return res.view({ aluno: alu[0], editavel: true });
            }
        });
    },
    index: (req, res) => {
        Utfpraluno.find().sort("nome").exec((err, alu) => {
            if (!err) {
                return res.view({ alunos: alu });
            } else {
                return res.view({ alunos: null });
            }
        });
    },
    create: (req, res) => {
        let id = req.param("id");
        let a = {
            nome: req.param("nome"),
            curso: req.param("curso"),
            ra: req.param("ra"),
            celular: req.param("celular"),
            email: req.param("email")
        }; 
        if (id) {
            Utfpraluno.update(parseInt(id), a, (err, alu) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar Aluno da UTFPR, info:`, 'error');
                    return res.redirect('/aluno/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/aluno');
                }
            });
        } else {
            Utfpraluno.create(a, (err, alu) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar Aluno da UTFPR, info:`, 'error');
                    return res.redirect('/aluno/new');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/aluno');
                }
            });
        }
    },
    destroy: (req, res) => {
        Utfprsaluno.destroy(id, (err, serv) => {
            flashService.createFlashPadrao(req, 8);
            return res.redirect('/aluno');
        });
    }
}

