const moment = require('moment');
const bluebird = require('bluebird');

module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    index: (req, res) => {
        var salario = 0;
        var id = req.param("user")
        return new Promise((success, reject) => {
            if (id) {
                Funcionario.find(id).exec((err, usuario) => {
                    var valorh = usuario[0].vhora;
                    valorh/= 60;
                    Ponto.find({id_func: usuario[0].id}).exec((err, marcacoes) => {
                        if (marcacoes) {
                            bluebird.each(marcacoes, (marca) => {
                                var totalhoras = marca.totalhoras;
                                var hora_inicio = marca.hora_inicio;
                                var hora_inicioalmoco = marca.hora_inicioalmoco;
                                var hora_fimalmoco = marca.hora_fimalmoco;
                                var hora_fim = marca.hora_fim;
                                salario += totalhoras * valorh;
                                if (totalhoras > 510) {
                                    salario += ((totalhoras - 510) * 0.3) * valorh
                                }
                                if (hora_inicio > 1320) {
                                    if (hora_fim > 1800) {
                                        salario += (((hora_fim - hora_inicio) - (hora_fimalmoco - hora_inicioalmoco) - (hora_fim - 1800)) * 0.2) * valorh;
                                    } else {
                                        salario += (((hora_fim - hora_inicio) - (hora_fimalmoco - hora_inicioalmoco)) * 0.2) * valorh;
                                    }
                                } else if (hora_fim > 1320) {
                                    if (hora_fim > 1800) {
                                        salario += (((hora_fim - 1320) - (hora_fimalmoco - hora_inicioalmoco) - (hora_fim - 1800)) * 0.2) * valorh;
                                    } else {
                                        salario += (((hora_fim - 1320) - (hora_fimalmoco - hora_inicioalmoco)) * 0.2) * valorh;
                                    }
                                }
                            }).then(() => {
                                return success();
                            });
                        }
                    });
                });
            } else {
                return success();
            }
        }).then(() => {

            Funcionario.find().sort("nome").exec((err, usuarios) => {
                if (id) {
                    return res.view({ usuarios: usuarios, userSelect: id, salario: salario });
                } else {
                    return res.view({ usuarios: usuarios, userSelect: null, salario: null });
                }
            });
        });
    },
    create: (req, res) => {

        var vhora = 0.0;
        vhora = parseFloat(req.param("valor_hora"));


        var f = {
            nome: req.param('nome'),
            cpfcnpj: req.param('cpfcnpj'),
            rua: req.param('rua'),
            numero: req.param('numero'),
            bairro: req.param('bairro'),
            cidade: req.param('cidade'),
            pais: req.param('pais'),
            complemento: req.param('complemento'),
            banco: req.param('banco'),
            agencia: req.param('agencia'),
            conta: req.param("conta"),
            vhora: vhora,
            operacao: req.param("operacao")

        };
        Funcionario.create(f, (err, funci) => {

            if (err) {
                flashService.createSessionFlash(req, `Erro ao criar a Funcionario, info:`, 'error');
                return res.redirect('/funcionario/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/funcionario');
            }
        });
    },
};

