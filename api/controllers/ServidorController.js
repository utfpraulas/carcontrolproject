module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    edit: (req, res) => {
        let id = req.param('id');
        let isEdit = req.param("is_edit");            
        Utfprservidor.find(id).exec((err, servidor) => {
            if (!isEdit) {
                return res.view({ servidor: servidor, editavel: false });
            } else {
                return res.view({ servidor: servidor, editavel: true });
            }
        });
    },
    index: (req, res) => {
        Utfprservidor.find().sort("nome").exec((err, servidores) => {
            if (!err) {
                return res.view({ servidores: servidores });
            } else {
                return res.view({ servidores: null });
            }
        });
    },
    create: (req, res) => {
        let id = req.param("idservidor");
        let servidorutfpr = {
            nome: req.param("nome"),
            telefone: req.param("telefone"),
            cargo: req.param("cargo"),
            matricula: req.param("matricula"),
            celular: req.param("celular"),
            email: req.param("email"),
        };

        if (req.param("ramal")) {
            servidorutfpr.ramal = req.param("ramal");
        } else {
            servidorutfpr.ramal = 0000;
        }
        if (req.param("sala")) {
            servidorutfpr.sala = req.param("sala");
        } else {
            servidorutfpr.sala = "";
        }
        if (id) {
            Utfprservidor.update(id, (err, serv) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao atualizar Servidor da UTFPR, info:`, 'error');
                    return res.redirect('/servidor/edit');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/servidor');
                }
            });
        } else {
            Utfprservidor.create(servidorutfpr, (err, serv) => {
                if (err) {
                    flashService.createSessionFlash(req, `Erro ao cadastrar Servidor da UTFPR, info:`, 'error');
                    return res.redirect('/servidor/new');
                } else {
                    flashService.createFlashPadrao(req, 1);
                    return res.redirect('/servidor');
                }
            });
        }
    },
    destroy: (req, res) => {
        Utfprservidor.destroy(id, (err, serv) => {
            flashService.createFlashPadrao(req, 8);
            return res.redirect('/servidor');
        });
    }
}

