module.exports = {
    new: (req, res) => {
        return res.view({});
    },
    index: (req, res) => {
        Veiculo.find().exec((err, vec) => {
            return res.view({veiculos: vec});
        });
    },
    create: (req, res) => {
        var v = {
            tipo: req.param('tipo'),
            modelo: req.param('modelo'),
            fabricante: req.param('fabricante'),
            placa: req.param('placa'),
            cor: req.param('cor')
        };
        Veiculo.create(v, (err, vec) => {

            if (err) {
                flashService.createSessionFlash(req, `Erro ao salvar Veiculo, info:`, 'error');
                return res.redirect('/veiculo/new');
            } else {
                flashService.createFlashPadrao(req, 1);
                return res.redirect('/veiculo');
            }
        });
    },
};

